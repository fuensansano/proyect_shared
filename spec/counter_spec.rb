require_relative "../src/counter"

describe Counter do
    it 'initially set to 0' do

        counter = Counter.new

        expect(counter.to_i).to eq(0)
    end

    it 'increase by 1' do
        counter = Counter.new

        counter.increase

        expect(counter.to_i).to eq(1)
    end

    it 'set all counters to maximum value' do
        counter = Counter.new

        counter.max_counter(7)

        expect(counter.to_i).to eq(7)
    end

end
