class Counters
    def initialize(counters_quantity)
        @values = build_counters(counters_quantity)
    end

    def to_a
        @values.map do |value|
            value.to_i
        end 
    end

    def increase(counter_position)
        adapted_position = counter_position - 1
        @values[adapted_position].increase
    end

    private

    def build_counters(counters_quantity)
        values = Array.new()

        counters_quantity.times do
            values.push(Counter.new)
        end
        values
    end
end

describe Counters do
    it 'given 5 counters, initially set to 0' do

        counters = Counters.new(5)

        expect(counters.to_a).to eq([0,0,0,0,0])
    end
    it 'given 3 counters, initially set to 0' do

        counters = Counters.new(3)

        expect(counters.to_a).to eq([0,0,0])
    end
    it 'increases the given counter position' do

        counters = Counters.new(3)

        counters.increase(2)

        expect(counters.to_a).to eq([0,1,0])
    end
    it 'increases the first counter position' do

        counters = Counters.new(3)

        counters.increase(1)

        expect(counters.to_a).to eq([1,0,0])
    end
end
